#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#define String char*

String txtfromfile(char* filename) //keep
{
    int length = 0;
    FILE *file;
    file = fopen(filename,"r");
    if(file == NULL)
    {
        printf("Error opening file %s ",filename);
    }
    else
    {
        while(getc(file) != EOF)
        {
            length++;
        }
        rewind(file);
        String text = malloc(length*sizeof(char));
        for(int i = 0; i < length; i++)
        {
            text[i] = getc(file);
        }
        fclose(file);
        return text;
    }
    return "error!";
}
 
 int zaehlebuchstaben(int list[], char* text)
 {
     int a = strlen(text);
     for (int i=0; i<26;i++)
     {
        for (int zaehler = 0; zaehler < a ; zaehler++ )
        {
            if(text[zaehler]==('A'+i))
            {
                list[i]++;        
            }       
        }
    }
}
void toLower (char* str){
    int len = strlen(str);
    for (int i = 0; i <len ; i++)
    {
        str[i] = tolower(str[i]);
    }
    
}
void toUpper (char* str){
    int len = strlen(str);
    for (int i = 0; i <len ; i++)
    {
        str[i] = toupper(str[i]);
    }
    
}


void swap(int list[],int a,int b){
    int s = list[a];
    list[a] = list[b];
    list[b] = s;
}

void char_swap(char list[],int a,int b){
    char s = list[a];
    list[a] = list[b];
    list[b] = s;
}

int groesste(int list[],int start, int llaen){
    int pos = start;
    int hiscore = list[pos];

    for (int i = start+1; i < llaen; i++)
    {
        if(list[i] > hiscore){
            hiscore = list[i];
            pos = i;
        }   
    }

    return pos;
}
void sortiere(int list[],char buch[], int llaen){
    for ( int i = 0; i < llaen; i++)
    {  
       char_swap (buch,i,groesste(list,i,llaen));
       swap (list,i,groesste(list,i,llaen)); 
    }
}

void exchange(char* text,char tochange,char exchange_with)
{
    int a = strlen(text);
    for (int i = 0; i < a; i++)
    {
        if (text[i]==tochange)
        {
            text[i] = exchange_with;
        }
    }
}

int main()
{
    char* a = txtfromfile("hpscrambled.txt");
    //char a[] = "dies hier ist ein text";
    toUpper(a);
    
    char buch[] = "abcdefghijklmnopqrstuvwxyz";
    int list[26] ={0};
    zaehlebuchstaben(list, a);
    sortiere(list,buch,26);
    //exchange(a,)
    for (int i = 0; i < 26; i++)
    {
        printf("%d, %c\n", list[i],buch[i]);
    }
    
    return 0;
}

